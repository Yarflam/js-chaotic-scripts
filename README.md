# JS Chaotic Scripts

My little scripts in bulk without taking my head too much to put them away. Some scripts are just ... drafts ! :D

## Index

-   AI: _Artificial Intelligence study & tests_
    1. Semantic vector
    2. Small FlappyBird AI
-   Network: _expose data, broadcast, manage packets and more_
    1. Capture the video and stream
    2. Test RTMP protocol
    3. Small instance with youtube-dl (local only, not commit today)
-   Scripts
    -   term: scripts must be executed from the command line
        1. Applying Pythagore theorem on many dimensions
    -   utils: some tools to use in front-end or back-end.
        1. Crypto test
        2. Manipulate Datetime
        3. Input for terminal (+ history commands)
-   Study: _all types of small projects to test_
    1. Algorithm generator - is it possible?
    2. Funny Happy Birthday, I sent to my friend
    3. Packer - try to organize many boxes to fit them (math problem)
    4. Simulation - break, I want to simulate some univers but I don't know what I want now, so ... x)

## Author

-   Yarflam - _initial worker_

## License

-   **MIT** : http://opensource.org/licenses/MIT
