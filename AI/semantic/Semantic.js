const SmtcVector = require('./SmtcVector');

class Semantic {
    constructor() {
        this._features = [];
        this._ifeat = [];
        this._maxLen = 0;
    }

    embedding(words) {
        let finder;
        let vector = new SmtcVector(this);
        if (typeof words === 'string') words = words.split(' ');
        for (let word of words) {
            if (!word.length) continue;
            finder = this._features.indexOf(word);
            if (finder < 0) {
                finder = this._features.length;
                this._features.push(word);
                this._ifeat.push(finder >> 0);
            }
            vector.add(finder);
            this._maxLen = Math.max(this._maxLen, word.length);
        }
        return vector;
    }
}

module.exports = Semantic;
