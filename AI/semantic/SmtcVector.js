class SmtcVector {
    constructor(semantic) {
        this._semantic = semantic;
        this._value = [];
    }

    add(feature) {
        this._value.push(feature);
    }
}

module.exports = SmtcVector;
