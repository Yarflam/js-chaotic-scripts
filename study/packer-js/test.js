const { Packer } = require('./packer');

const blocks = [
    { n: 0, w: 40, h: 20 },
    { n: 1, w: 50, h: 40 },
    { n: 2, w: 110, h: 20 }
];

const packer = new Packer(150, 60);
packer.fit(blocks);

console.log(
    blocks.map(({ n, w, h, fit }) => {
        return { n, x: fit.x, y: fit.y, w, h };
    })
);

// console.log(
//     JSON.stringify(
//         new _BinPacker(150, 60)
//             .fit([
//                 { n: 0, w: 40, h: 20 },
//                 { n: 1, w: 50, h: 40 },
//                 { n: 2, w: 110, h: 20 }
//             ])
//             .map(({ n, w, h, fit }) => {
//                 return { n, x: fit.x, y: fit.y, w, h };
//             }),
//         false,
//         4
//     )
// );
