class GenAlgo {
    constructor({ init, evaluate, turn, finish }) {
        this._v = 1;
        this._nb = init.nb;
        this._factory = init.factory;
        this._time = evaluate.time;
        this._action = evaluate.action;
        this._fitness = evaluate.fitness;
        this._keep = turn.keep;
        this._filter = turn.filter;
        this._combine = turn.combine;
        this._cycles = finish.cycles;
    }

    run() {
        let sim = {};
        if (this._v) console.log('New Simulation');
        /* 1. Initialize */
        if (this._v) console.log('1. Initialize');
        sim.pop = new Array(this._nb).fill(0).map((_, i) => this._factory(i));
        if (this._v) console.log(`-> ${this._nb} objects created`);
        /* 2. Evaluate */
        if (this._v) console.log('2. Evaluate');
        const nbSteps = Math.round(this._time / 10);
        for (let t = 0; t < this._time; t++) {
            if (this._v && !(t % nbSteps))
                console.log(`Time ${t} -> ${t + nbSteps}`);
            sim.pop = sim.pop.map(
                (obj, i) => this._action(obj, i, sim.pop) || obj
            );
        }
        if (this._v) console.log('Calculate fitness');
        sim.fitness = sim.pop.map(
            (obj, i) => this._fitness(obj, i, sim.pop) || 0
        );
    }

    /* Tools */

    static rand(a, b) {
        return Math.random() * (b - a) + a;
    }

    static randInt(a, b) {
        return Math.floor(GenAlgo.rand(a, b));
    }

    static limitArea(x, a, b) {
        return x >= a && x <= b ? x : x < a ? a : b;
    }

    static distVector2d([ax, ay], [bx, by]) {
        return Math.sqrt((bx - ax) * (bx - ax) + (by - ay) * (by - ay));
    }
}

module.exports = GenAlgo;
