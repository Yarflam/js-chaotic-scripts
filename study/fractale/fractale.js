let { Canvas } = require('canvas');

let canvas = new Canvas(500, 500);
let ctx = canvas.getContext('2d');

let width = canvas.width;
let height = canvas.height;

let maxIteration = 200;

let zoom = 0.05;
let moveX = 0.4 + Math.random() * 0.05;
let moveY = -0.2 + Math.random() * 0.05;
let xmin = -1.5 * zoom + moveX;
let xmax = 1.5 * zoom + moveX;
let ymin = -1 * zoom + moveY;
let ymax = 1 * zoom + moveY;

let x = 0;
let y = 0;

let x0 = 0;
let y0 = 0;

let xnew = 0;
let ynew = 0;

let iteration = 0;

let colorR = 0;
let colorG = 0;
let colorB = 0;

for (let i = 0; i < width; i++) {
    for (let j = 0; j < height; j++) {
        x = xmin + i * (xmax - xmin) / width;
        y = ymin + j * (ymax - ymin) / height;

        x0 = 0;
        y0 = 0;

        iteration = 0;

        while (x0 * x0 + y0 * y0 < 4 && iteration < maxIteration) {
            xnew = x0 * x0 - y0 * y0 + x;
            ynew = 2 * x0 * y0 + y;

            x0 = xnew;
            y0 = ynew;

            iteration++;
        }

        colorR = iteration * 100 / maxIteration * 2;
        colorG = iteration * 200 / maxIteration * 2;
        colorB = iteration * 255 / maxIteration * 2;

        ctx.fillStyle = 'rgb(' + colorR + ',' + colorG + ',' + colorB + ')';
        ctx.fillRect(i, j, 1, 1);
    }
}

ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
ctx.fillRect(width - 105, height - 20, 105, 20);

ctx.fillStyle = 'white';
ctx.fillText(moveX.toFixed(6) + ' ' + moveY.toFixed(6), width - 101, height - 6);

let fs = require('fs');
let out = fs.createWriteStream(__dirname + '/out.jpg');
let stream = canvas.createJPEGStream({
    bufsize: 4096,
    quality: 100
});

stream.pipe(out);
out.on('finish', function () {
    console.log('The JPEG file was created.');
});