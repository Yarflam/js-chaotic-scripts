const sleep = () => new Promise(resolve => setTimeout(resolve, 30));
let map = {};
const mtx = {
    xmin: -1,
    xmax: 1,
    ymin: -1,
    ymax: 1
};
function showMap() {
    mtx.w = mtx.xmax - mtx.xmin + 1;
    mtx.h = mtx.ymax - mtx.ymin + 1;
    /* Show */
    console.log('>');
    for (let ymax = mtx.h, y = 0; y < ymax; y++) {
        console.log(
            new Array(mtx.w)
                .fill(0)
                .map(
                    (_, x) =>
                        (map[`${mtx.xmin + x}:${mtx.ymin + y}`] || {}).d ||
                        String.fromCharCode(9618)
                )
                .join('')
        );
    }
}
function saveMap(x, y, data) {
    const view = '10|01|21|12'.split('|');
    for (let i = 0; i < data.length; i++) {
        const pos = {
            x: x + Number(view[i][0]) - 1,
            y: y + Number(view[i][1]) - 1,
            d: data[i]
        };
        map[`${pos.x}:${pos.y}`] = pos;
        /* Update */
        mtx.xmin = Math.min(mtx.xmin, pos.x - 1);
        mtx.xmax = Math.max(mtx.xmax, pos.x + 1);
        mtx.ymin = Math.min(mtx.ymin, pos.y - 1);
        mtx.ymax = Math.max(mtx.ymax, pos.y + 1);
    }
    map[`${x}:${y}`] = { x, y, d: 'o' };
}

async function m(
    c,
    r = 4096,
    o = {
        a: '_',
        b: '#',
        p: '10|01|21|12'
    },
    s = { x: 0, y: 0, i: 0 }
) {
    o.k = `${o.a}${o.b}`;
    s._o = v => {
        let nb =
            Math.abs(
                Math.floor(r * Math.sin(r + v[0]) * Math.cos(v[0] + v[1]) + r)
            ) % 20;
        return nb >= 7 ? o.a : o.b;
    };
    while (s._o([0, 0]) !== o.a) {
        r++;
        if (r >= 1e6) return;
    }
    s._d = () =>
        (s.d = o.p
            .split('|')
            .map(v => s._o([s.x + Number(v[0]) - 1, s.y + Number(v[1]) - 1])));
    s._d();
    while (1) {
        saveMap(s.x, s.y, s.d);
        s.i = c(s.d);
        if (typeof s.i !== 'number') break;
        await sleep();
        if (s.d[s.i] !== o.a) continue;
        // console.log(`x: ${s.x}, y: ${s.y}`);
        // console.log(`action: ${s.i}`);
        s[(s.i + 1) % 4 < 2 ? 'y' : 'x'] += s.i < 2 ? -1 : 1;
        s._d();
    }
}

function myCode() {
    /* Positions */
    let x = 0;
    let y = 0;
    let lockMove = '';
    let wall = 'R';

    /* Map entities */
    const FREE = '_';
    const BLOCK = '#';
    const ALL = [FREE, BLOCK];

    /* Utils */
    const getKey = action => [0, 1, 2, 3]['TLRB'.indexOf(action)];

    /* Move in direction */
    function move(action, step) {
        console.log(`Action: ${action} |${step}`);
        lockMove = 'BRLT'[getKey(action)];
        return getKey(action);
    }

    /* Run */
    m(screen => {
        showMap(); // cheat
        // Do something
        /* 1 - Look at left */
        let see = 'LBTR'[getKey(wall)];
        if (
            see !== lockMove &&
            screen[getKey(see)] === FREE &&
            Math.random() > 0.5
        ) {
            console.log(`WALL ${wall} -> ${see}`);
            wall = see;
        }
        /* 2 - Move */
        let direct = String(wall);
        if (screen[getKey(direct)] === FREE) return move(direct, 1);
        /* 3 - Next wall */
        direct = 'RTBL'[getKey(direct)];
        if (screen[getKey(direct)] === FREE) return move(direct, 2);
        /* 4 - Last chances */
        for (let i = 0; i < 2; i++) {
            direct = 'RTBL'[getKey(direct)];
            wall = String(direct);
            if (screen[getKey(direct)] === FREE) return move(direct, 3);
        }
    }, 1620);
    // bug_map: 200
}

myCode();
