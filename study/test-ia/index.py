import random


class Character:
    def __init__(self, name, level, race, char_class, background, alignment, strength, dexterity, constitution,
                 intelligence, wisdom, charisma):
        self.name = name
        self.level = level
        self.race = race
        self.char_class = char_class
        self.background = background
        self.alignment = alignment
        self.strength = strength
        self.dexterity = dexterity
        self.constitution = constitution
        self.intelligence = intelligence
        self.wisdom = wisdom
        self.charisma = charisma


class Item:
    def __init__(self, name, description, value):
        self.name = name
        self.description = description
        self.value = value


def generate_character(name, level, race, char_class, background, alignment, strength, dexterity, constitution,
                       intelligence, wisdom, charisma):
    character = Character(name, level, race, char_class, background, alignment, strength, dexterity, constitution,
                          intelligence, wisdom, charisma)
    print("Your character's name is: " + character.name)
    print("Your character's level is: " + str(character.level))
    print("Your character's race is: " + character.race)
    print("Your character's class is: " + character.char_class)
    print("Your character's background is: " + character.background)
    print("Your character's alignment is: " + character.alignment)
    print("Your character's strength is: " + str(character.strength))
    print("Your character's dexterity is: " + str(character.dexterity))
    print("Your character's constitution is: " + str(character.constitution))
    print("Your character's intelligence is: " + str(character.intelligence))
    print("Your character's wisdom is: " + str(character.wisdom))
    print("Your character's charisma is: " + str(character.charisma))


generate_character(input("Enter your character's name: "), int(input("Enter your character's level: ")),
                   input("Enter your character's race: "), input(
                       "Enter your character's class: "),
                   input("Enter your character's background: "), input(
                       "Enter your character's alignment: "),
                   random.randint(1, 20), random.randint(
                       1, 20), random.randint(1, 20), random.randint(1, 20),
                   random.randint(1, 20), random.randint(1, 20))
