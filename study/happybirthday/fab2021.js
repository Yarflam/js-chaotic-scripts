const QrCodeWriter = require('qrcode');
const base64 = require('base-64');
const path = require('path');

/* Payload */
const data = new Array(5 * 3 + 2)
    .fill(0)
    .map((_, i) => {
        if (i === 5) return base64.encode('Joyeux anniversaire Fabien ! <3');
        if (i === 11)
            return base64.encode('Je me sentais obligé cette année ah ah');
        if (i < 5) return 'Fabien';
        if (i < 5 * 2 + 1) return 'Moulet';
        return 'XXVI';
    })
    .join('');

console.log(data);

/* Export */
QrCodeWriter.toFile(path.resolve(__dirname, 'temp/fab2021.jpg'), [
    {
        data: new Buffer.from(data),
        mode: 'byte'
    }
]);
