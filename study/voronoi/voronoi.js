const { createCanvas } = require('canvas');
const Voronoi = require('voronoi');
const graphlib = require('graphlib');

const voronoi = new Voronoi();

const width = 1920;
const height = 1080;
const greenPart = 0.5;
const genParts = Math.min(width, height) * 2;

const center = {
    w: width/2,
    h: height/2,
    r: 1/Math.min(width*greenPart, height*greenPart)
};
function distCenter(x, y) {
    return Math.sqrt(
        (center.w - x) * (center.w - x) +
        (center.h - y) * (center.h - y)
    ) * center.r;
}

const canvas = createCanvas(width, height);
const ctx = canvas.getContext('2d');

const bbox = { xl: 0, xr: width, yt: 0, yb: height };
const sites = [];
for (let i = 0; i < genParts; i += 1) {
    sites.push({
        x: Math.floor(Math.random() * width),
        y: Math.floor(Math.random() * height)
    });
}
const diagram = voronoi.compute(sites, bbox);

ctx.fillStyle = '#FFFFFF';
ctx.fillRect(0, 0, width, height);

if (diagram) {
    const cells = diagram.cells;
    const blackSites = [];
    for (let i = 0; i < cells.length; i += 1) {
        const cell = cells[i];
        const site = cell.site;
        const halfedges = cell.halfedges;
        const length = halfedges.length;
        if (length > 2) {
            let v = halfedges[0].getStartpoint(); // eslint-disable-line prefer-const
            ctx.fillStyle = '#003377';
            // site.x > width*greenPart && site.x < width*(1-greenPart) && site.y > height*greenPart && site.y < height*(1-greenPart) &&
            const isGreen = Math.random() > distCenter(site.x, site.y);
            if (isGreen) { // eslint-disable-line max-len
                const gradient = ctx.createLinearGradient(0, 0, width, height);
                gradient.addColorStop(0, '#00AA00');
                gradient.addColorStop(1, '#003300');
                ctx.fillStyle = gradient;
            }
            ctx.beginPath();
            ctx.moveTo(v.x, v.y);
            for (let j = 0; j < length; j += 1) {
                v = halfedges[j].getEndpoint();
                ctx.lineTo(v.x, v.y);
            }
            ctx.fill();
            if (isGreen) {
                ctx.fillStyle = '#000000';
                ctx.fillRect(site.x-3, site.y-3, 6, 6);
                blackSites.push(site);
            }
        }
    }
    const g = new graphlib.Graph();
    for (let i = 0; i < blackSites.length; i += 1) {
        g.setNode(i, blackSites[i]);
    }
    for (let i = 0; i < blackSites.length; i += 1) {
        for (let j = i + 1; j < blackSites.length; j += 1) {
            g.setEdge(i, j);
        }
        const wrongPath = Object.entries(
            graphlib.alg.dijkstra(g, i, ({ v, w }) => {
                const source = g.node(v);
                const target = g.node(w);
                return Math.sqrt(Math.pow(source.x - target.x, 2) + Math.pow(source.y - target.y, 2));
            })
        )
            .filter(([k]) => Number(k) > i)
            .sort(([,a], [,b]) => ( a.distance <= b.distance ? -1 : 1 ))
            .slice(1);
        for(let path of wrongPath) g.removeEdge(i, path[0]);
    }
    // console.log(distances)
    const edges = g.edges();
    for (let i = 0; i < edges.length; i += 1) {
        const edge = edges[i];
        const source = g.node(edge.v);
        const target = g.node(edge.w);
        ctx.beginPath();
        ctx.moveTo(source.x, source.y);
        ctx.lineTo(target.x, target.y);
        ctx.stroke();
    }
}

const fs = require('fs');
const out = fs.createWriteStream(__dirname + '/voronoi.jpg');
const stream = canvas.createJPEGStream({
    bufsize: 4096,
    quality: 1
});

stream.pipe(out);
out.on('finish', function () {
    console.log('The JPEG file was created.');
});