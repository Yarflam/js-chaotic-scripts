const { createCanvas } = require('canvas');
const Voronoi = require('voronoi');

const voronoi = new Voronoi();

const canvas = createCanvas(500, 500);
const ctx = canvas.getContext('2d');

const bbox = { xl: 0, xr: 500, yt: 0, yb: 500 };
const sites = [];
for (let i = 0; i < 100; i += 1) {
    sites.push({
        x: Math.floor(Math.random() * 500),
        y: Math.floor(Math.random() * 500)
    });
}
const diagram = voronoi.compute(sites, bbox);

ctx.fillStyle = '#FFFFFF';
ctx.fillRect(0, 0, 500, 500);

if (diagram) {
    const cells = diagram.cells;
    const blackSites = [];
    for (let i = 0; i < cells.length; i += 1) {
        const cell = cells[i];
        const site = cell.site;
        const halfedges = cell.halfedges;
        const length = halfedges.length;
        if (length > 2) {
            let v = halfedges[0].getStartpoint(); // eslint-disable-line prefer-const
            ctx.fillStyle = '#003377';
            const isGreen = site.x > 50 && site.x < 400 && site.y > 100 && site.y < 400;
            if (isGreen) { // eslint-disable-line max-len
                const gradient = ctx.createLinearGradient(0, 0, 500, 500);
                gradient.addColorStop(0, '#00AA00');
                gradient.addColorStop(1, '#003300');
                ctx.fillStyle = gradient;
            }
            ctx.beginPath();
            ctx.moveTo(v.x, v.y);
            for (let j = 0; j < length; j += 1) {
                v = halfedges[j].getEndpoint();
                ctx.lineTo(v.x, v.y);
            }
            ctx.fill();
            if (isGreen) {
                ctx.fillStyle = '#000000';
                ctx.fillRect(site.x, site.y, 5, 5);
                blackSites.push(site);
            }
        }
    }
    ctx.beginPath();
    ctx.moveTo(blackSites[0].x, blackSites[0].y);
    for (let i = 1; i < blackSites.length; i += 1) {
        ctx.lineTo(blackSites[i].x, blackSites[i].y);
    }
    ctx.stroke();
}

const fs = require('fs');
const out = fs.createWriteStream(__dirname + '/voronoi.jpg');
const stream = canvas.createJPEGStream({
    bufsize: 4096,
    quality: 100
});

stream.pipe(out);
out.on('finish', function () {
    console.log('The JPEG file was created.');
});