import * as FlowConst from './FlowConst.mjs';
import WebSocket from 'ws';

class FlowClient {
    constructor() {
        this._url = null;
        this._handle = null;
    }

    connect(url=this._url) {
        this._url = url;
        /* Connect */
        this._handle = new WebSocket(url, { perMessageDeflate: false });
    }

    publish(channel, callback) {
        const chan = new FlowChan(this, channel).publish();
        if(typeof callback === 'function') callback(chan);
        return chan;
    }

    subscribe(channel, callback) {
        const chan = new FlowChan(this, channel).subscribe();
        if(typeof callback === 'function') callback(chan);
        return chan;
    }

    getSocket() {
        return this._handle;
    }
}
FlowClient.CHAN_BROADCAST = FlowConst.CHAN_BROADCAST;
FlowClient.TOPIC_BROADCAST = FlowConst.TOPIC_BROADCAST;

/* Control chan */
class FlowChan {
    constructor(client, channel) {
        this._client = client;
        this._channel = channel;
        this._mode = null;
    }

    publish() {
        this._mode = FlowConst.FLOW_PUBLISH;
        const socket = this._client.getSocket();
        socket.on(FlowConst.WS_CONNECTION, () => {
            socket.send(FlowConst.FLOW_PUBLISH, this._channel);
        });
        return this;
    }

    subscribe() {
        this._mode = FlowConst.FLOW_SUBSCRIBE;
        const socket = this._client.getSocket();
        socket.on(FlowConst.WS_CONNECTION, () => {
            socket.send(FlowConst.FLOW_SUBSCRIBE, this._channel);
        });
        return this;
    }
}

export default FlowClient;