import * as FlowConst from './FlowConst.mjs';
import { WebSocketServer } from 'ws';
import uniqid from 'uniqid';
import 'colors';

class FlowServer {
    constructor() {
        this._host = null;
        this._port = null;
        this._handle = null;
        this._clients = [];
    }

    listen(host=this._host, port=this._port, callback) {
        this._host = host;
        this._port = port;
        /* Create the Websocket Server */
        this._handle = new WebSocketServer({
            host: this._host,
            port: this._port
        });
        this._handle.on(FlowConst.WS_CONNECTION, client => {
            client.id = uniqid();
            console.log(`[HOST] Connected ${client.id}`.white);
            client.on('close', () => {
                console.log(`[HOST] Disconnected ${client.id}`.grey);
            });
        })
        /* Callback */
        if(typeof callback === 'function') callback();
    }
}

export default FlowServer;