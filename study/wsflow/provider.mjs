import FlowServer from './FlowServer.mjs';

/* Create my awesome Flow Server (hosting) */
const server = new FlowServer();
server.listen('127.0.0.1', 3033, () => {
    console.log('Provider is running.');
});