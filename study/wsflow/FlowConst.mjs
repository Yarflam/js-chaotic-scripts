export const WS_CONNECTION = 'connection';
export const WS_CLOSE = 'close';

export const FLOW_PUBLISH = 'FLOW@PUBLISH';
export const FLOW_SUBSCRIBE = 'FLOW@SUBSCRIBE';

export const FLOW_CHAN_BROADCAST = '@';
export const FLOW_TOPIC_BROADCAST = '@';