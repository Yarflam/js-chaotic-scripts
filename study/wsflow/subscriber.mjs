import FlowClient from './FlowClient.mjs'

/* Connect and subscribe */
const flow = new FlowClient();
flow.connect('ws://127.0.0.1:3033');
flow.subscribe('main', chan => {
    chan.send(FlowClient.TOPIC_BROADCAST, 'test');
});