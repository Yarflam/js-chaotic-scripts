const SHA256 = require('crypto-js/sha256');
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

class Wallet {
    constructor() {
        this.balance = 0;
        this.keyPair = ec.genKeyPair();
        this.publicKey = this.keyPair.getPublic().encode('hex');
    }

    toString() {
        return `Wallet -
            publicKey: ${this.publicKey.toString().substring(0, 16)}...
            balance  : ${this.balance}`
    }
}

class Transaction {
    constructor(fromAddress, toAddress, amount) {
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.amount = amount;
    }

    toString() {
        return `Transaction:
            from: ${this.fromAddress}
            to  : ${this.toAddress}
            amount: ${this.amount}`
    }

    calculateHash() {
        return SHA256(this.fromAddress + this.toAddress + this.amount).toString();
    }

    signTransaction(signingKey) {
        if (signingKey.getPublic('hex') !== this.fromAddress) {
            throw new Error('You cannot sign transactions for other wallets!');
        }

        const hashTx = this.calculateHash();
        const sig = signingKey.sign(hashTx, 'base64');
        this.signature = sig.toDER('hex');
    }

    isValid() {
        if (this.fromAddress === null) return true;

        if (!this.signature || this.signature.length === 0) {
            throw new Error('No signature in this transaction');
        }

        const publicKey = ec.keyFromPublic(this.fromAddress, 'hex');
        return publicKey.verify(this.calculateHash(), this.signature);
    }
}

class Block {
    constructor(index, timestamp, transactions, previousHash = '') {
        this.index = index;
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.previousHash = previousHash;
        this.nonce = 0;
        this.hash = this.calculateHash();
    }

    mineBlock(difficulty) {
        while (this.hash.substring(0, difficulty) !== new Array(difficulty + 1).join('0')) {
            this.hash = this.calculateHash();
            this.nonce++;
        }
        console.log('BLOCK MINED: ' + this.hash);
    }

    calculateHash() {
        return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.transactions) + this.nonce).toString();
    }
}

class Blockchain {
    constructor() {
        this.difficulty = 1;
        this.miningReward = 100;
        this.pendingTransactions = [];
        this.chain = [this.createGenesisBlock()];
    }

    createGenesisBlock() {
        return new Block(0, '01/01/2017', 'Genesis block', '0');
    }

    getLatestBlock() {
        return this.chain[this.chain.length - 1];
    }

    minePendingTransactions(miningRewardAddress) {
        let block = new Block(this.chain.length, Date.now(), this.pendingTransactions);
        block.mineBlock(this.difficulty);

        console.log('Block successfully mined!');
        this.chain.push(block);

        this.pendingTransactions = [
            new Transaction(null, miningRewardAddress, this.miningReward)
        ];
    }

    createTransaction(transaction) {
        this.pendingTransactions.push(transaction);
    }

    getBalanceOfAddress(address) {
        let balance = 0;

        for (const block of this.chain) {
            for (const trans of block.transactions) {
                if (trans.fromAddress === address) {
                    balance -= trans.amount;
                }

                if (trans.toAddress === address) {
                    balance += trans.amount;
                }
            }
        }

        return balance;
    }

    isChainValid() {
        for (let i = 1; i < this.chain.length; i++){
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if (currentBlock.hash !== currentBlock.calculateHash()) {
                return false;
            }

            if (currentBlock.previousHash !== previousBlock.hash) {
                return false;
            }
        }

        return true;
    }
}

let savjeeCoin = new Blockchain();

let walletA = new Wallet();
let walletB = new Wallet();

let transaction = new Transaction(walletA.publicKey, walletB.publicKey, 5);
transaction.signTransaction(walletA.keyPair);

console.log('Is transaction valid?', transaction.isValid());

savjeeCoin.createTransaction(transaction);

console.log('Starting the miner...');
savjeeCoin.minePendingTransactions(walletA.publicKey);

console.log('Balance of walletA:', savjeeCoin.getBalanceOfAddress(walletA.publicKey));
console.log('Balance of walletB:', savjeeCoin.getBalanceOfAddress(walletB.publicKey));

console.log('Starting the miner again...');
savjeeCoin.minePendingTransactions(walletA.publicKey);

console.log('Balance of walletA:', savjeeCoin.getBalanceOfAddress(walletA.publicKey));
console.log('Balance of walletB:', savjeeCoin.getBalanceOfAddress(walletB.publicKey));