import fs from 'fs';

class Mesh {
    constructor(name) {
        this._name = name;
        this._v = [];
        this._f = [];
    }

    import (pack) {
        let start = this.getVSize();
        this._v.push(...(pack.v||[]));
        this._f.push(...(pack.f||[]).map(
            m => m.map(index => index + start)
        ));
    }

    exportObj () {
        let out = `o ${this._name}\n`;
        for(let v of this._v) {
            out += `v ${v.join(' ')}\n`;
            out += 'vt 0 0\n';
        }
        out += 'vn 0 0 0\n';
        out += 's 0\n';
        for(let f of this._f) {
            out += `f ${f.map(face => `${face+1}/${face+1}/1`).join(' ')}\n`;
        }
        return out;
    }

    position (x=0, y=0, z=0) {
        const center = this.getCenter();
        return this.moveXYZ(
            x-center[0], y-center[1], z-center[2]
        );
    }

    moveXYZ (x=0, y=0, z=0) {
        for(let v of this._v) {
            v[0] += x;
            v[1] += y;
            v[2] += z;
        }
        return this;
    }

    getCenter() {
        let pos = [0,0,0];
        for(let v of this._v) {
            pos[0] += v[0];
            pos[1] += v[1];
            pos[2] += v[2];
        }
        const vLen = this._v.length || 1;
        pos[0] /= vLen;
        pos[1] /= vLen;
        pos[2] /= vLen;
        return pos;
    }

    getName() {
        return this._name;
    }

    getVertices() {
        return this._v;
    }

    getFaces() {
        return this._f;
    }

    getVSize() {
        return this._v.length;
    }
}

class MultiMesh {
    constructor(name) {
        this._name = name;
        this._mesh = [];
    }

    exportObj() {
        let out = '';
        let idc = { v: 0, vt: 0, vn: 0 };
        for(let mesh of this._mesh) {
            out += `o ${mesh.getName()}\n`;
            for(let v of mesh.getVertices()) {
                out += `v ${v.join(' ')}\n`;
                out += 'vt 1.0 1.0\n';
            }
            out += 'vn 0 0 0\n';
            out += 's 0\n';
            for(let f of mesh.getFaces()) {
                out += `f ${f.map(face => `${idc.v+face+1}/${idc.vt+face+1}/${idc.vn+1}`).join(' ')}\n`;
            }
            /* Increment */
            idc.v += mesh.getVSize();
            idc.vt += mesh.getVSize();
            idc.vn++;
        }
        return out;
    }
}

/***************(o)***************/

class Cube extends Mesh {
    constructor(name, size=1) {
        super(name);
        this.__build(size);
    }

    __build(size) {
        const sz = size/2;
        let pack = {};
        pack.v = [
            [ -sz, -sz, -sz ],
            [ sz, -sz, -sz ],
            [ sz, sz, -sz ],
            [ -sz, sz, -sz ],
            [ -sz, -sz, sz ],
            [ sz, -sz, sz ],
            [ sz, sz, sz ],
            [ -sz, sz, sz ]
        ];
        pack.f = [
            [ 0, 1, 2, 3 ],
            [ 4, 5, 6, 7 ],
            [ 0, 1, 5, 4 ],
            [ 1, 2, 6, 5 ],
            [ 2, 3, 7, 6 ],
            [ 3, 0, 4, 7 ],
        ];
        this.import(pack);
        return this;
    }
}

class Pyramid extends MultiMesh {
    constructor(name, dim=10, sizeCube=1) {
        super(name);
        this._dim = Math.floor(dim/2)*2 + 1;
        this.__build(this._dim, sizeCube);
    }

    __build(dim, sizeCube, altitude=0) {
        let middle = Math.floor(dim/2);
        for(let i=0; i < dim; i++) {
            this._mesh.push(
                new Cube(`${this._name}_${i*2}`, sizeCube)
                    .position(
                        (i - middle) * sizeCube,
                        altitude * sizeCube,
                        -(i < middle ? i : dim - i - 1) * sizeCube
                    )
            );
            this._mesh.push(
                new Cube(`${this._name}_${(i*2)+1}`, sizeCube)
                    .position(
                        (i - middle) * sizeCube,
                        altitude * sizeCube,
                        (i < middle ? i : dim - i - 1) * sizeCube
                    )
            );
        }
        if(dim > 0) this.__build(dim-2, sizeCube, altitude+1);
    }
}

class Map extends Mesh {
    constructor(name, size=2, gap=1) {
        super(name);
        this._size = size;
        this.__build(size, gap);
    }

    __build(size, gap) {
        let zLevel = {};
        const middle = size / 2;
        for(let y=0; y < size; y++) {
            for(let x=0; x < size; x++) {
                if(typeof zLevel[`${x},${y}`] === 'undefined')
                    zLevel[`${x},${y}`] = Math.random() * gap;
                if(typeof zLevel[`${x+1},${y}`] === 'undefined')
                    zLevel[`${x+1},${y}`] = Math.random() * gap;
                if(typeof zLevel[`${x+1},${y+1}`] === 'undefined')
                    zLevel[`${x+1},${y+1}`] = Math.random() * gap;
                if(typeof zLevel[`${x},${y+1}`] === 'undefined')
                    zLevel[`${x},${y+1}`] = Math.random() * gap;
                // add
                this.import({
                    v: [
                        [ x - middle, zLevel[`${x},${y}`], y - middle ],
                        [ x+1 - middle, zLevel[`${x+1},${y}`], y - middle ],
                        [ x+1 - middle, zLevel[`${x+1},${y+1}`], y+1 - middle ],
                        [ x - middle, zLevel[`${x},${y+1}`], y+1 - middle ]
                    ],
                    f: [
                        [ 0, 1, 2, 3 ]
                    ]
                });
            }
        }
    }
}

class Cilinder extends Mesh {
    constructor(name, size=1, arcs=6) {
        super(name);
        this._size = size;
        this._arcs = arcs;
        this.__build(size, arcs);
    }

    __build(size, arcs) {
        let face = [];
        for(let i=0; i < arcs; i++) {
            this._v.push([
                Math.cos(2*Math.PI/arcs*i) * size * 0.5,
                0,
                Math.sin(2*Math.PI/arcs*i) * size * 0.5
            ]);
            face.push(i);
        }
        this._f.push(face);
        return this;
    }
}

class RayCilinder extends Mesh {
    constructor(name, stops=[], arcs=5, size=1) {
        super(name);
        this._size = size;
        this._arcs = arcs;
        this.__build(stops, arcs, size);
    }

    __build(stops, arcs, size) {
        let current, last=null;
        for(let stop of stops) {
            current = [];
            for(let i=0; i < arcs; i++) {
                current.push(this._v.length);
                this._v.push([
                    stop[0],
                    stop[1] + Math.sin(2*Math.PI/arcs*i) * size * 0.5,
                    stop[2] + Math.cos(2*Math.PI/arcs*i) * size * 0.5
                ]);
                if(last) {
                    this._f.push([
                        last[i],
                        last[(i+1) % arcs],
                        (i+1) < arcs ? current[i]+1 : current[(i+1) % arcs],
                        current[i],
                    ]);
                }
            }
            // this._f.push(current);
            last = [...current];
        }
        return this;
    }
}

function main() {
    let model;
    const rnd = Math.round(Math.random() * 4);
    switch(rnd) {
        case 0:
            model = new Cube('Cube', 1);
            break;
        case 1:
            model = new Pyramid('Pyramid', 10, 1);
            break;
        case 2:
            model = new Map('Map', 20, 2);
            break;
        case 3:
            model = new Cilinder('Cilinder', 1, 8);
            break;
        case 4:
            model = new RayCilinder('RC', [
                [ -1, 0, 0 ],
                [  0, 1, 0 ],
                [  1, 1, 0 ],
                [  2, 1.5, 0 ]
            ]);
            break;
    }
    /* Export to .obj */
    const obj = model.exportObj();
    fs.writeFileSync('./cube.obj', obj);
}
main();

export { Mesh, MultiMesh, Cube, Pyramid, Map };