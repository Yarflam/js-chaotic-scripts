const socketio = require('socket.io');
const express = require('express');
const http = require('http');

const HOST = '0.0.0.0';
const PORT = 3110;

/* Server Express */
let app = express();
let server = http.Server(app);
server.listen(PORT, HOST, function() {
    console.log(`The server working on ${HOST}:${PORT}.`);
});

/* Socket.io */
let socket = socketio(server);
socket.on('connection', socket => {
    console.log('ok');
});

app.get(/^\/?(.+)$/, (req, res) => {
    res.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
    res.write(
        JSON.stringify({
            success: true,
            error: false,
            data: 'server is running'
        })
    );
    res.end();
});
