function main() {
    /* Socket */
    const socket = io('http://127.0.0.1:3000');
    socket.on('connect', () => {
        console.log('[^] Socket connected');
    });
    socket.on('disconnect', () => {
        console.log('[v] Socket disconnected');
    });

    /* Canvas */
    const cvs = document.querySelector('canvas');
    const ctx = cvs.getContext('2d');
    cvs.width = cvs.offsetWidth;
    cvs.height = cvs.offsetHeight;

    /* Streaming */
    socket.on('myStream', data => drawStream(data, { cvs, ctx }));
}

function drawStream(data, { cvs, ctx }) {
    let img = new Image();
    img.src = `data:image/jpeg;base64,${data}`;
    img.onload = () => {
        let box = {
            iw: Math.max(img.width, img.height),
            ih: Math.min(img.width, img.height),
            cw: Math.max(cvs.width, cvs.height),
            ch: Math.min(cvs.width, cvs.height)
        };
        box.iw = 400;
        box.ih = 400;
        box.sw = (box.iw / box.ih) * box.ch;
        box.dx = (box.cw - box.sw) / 2;
        ctx.drawImage(img, 0, 0, box.iw, box.ih, box.dx, 0, box.sw, box.ch);
    };
}

window.addEventListener('load', () => main());
