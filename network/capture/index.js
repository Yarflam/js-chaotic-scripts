const { spawn } = require('child_process');
const { createServer } = require('http');
const { Server: SocketIO } = require('socket.io');
const ffmpeg = require('ffmpeg-static');

const HOST = '0.0.0.0';
const PORT = 3000;

/* Streaming */
const computer = spawn(
    ffmpeg,
    [
        '-video_size',
        '1920x1080',
        // '-probesize',
        // '100M',
        '-framerate',
        '60',
        '-f',
        'x11grab',
        '-i',
        ':1',
        // '/dev/video0',
        '-f',
        'mjpeg',
        '-'
    ],
    { stdio: 'pipe' }
);

/* Web Socket */
const server = createServer((req, res) => {
    // Set CORS headers
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', '*');
    if (req.method === 'OPTIONS') {
        res.writeHead(200);
        res.end();
        return;
    }
});
const ws = new SocketIO(server, {
    cors: (origin, callback) => callback(null, true)
});

let sockets = [];
ws.on('connection', socket => {
    sockets.push(socket);
});

computer.stderr.pipe(process.stdout);

computer.stdout.on('data', data => {
    const image = Buffer.from(data).toString('base64');
    sockets.forEach(socket => socket.emit('myStream', image));
});

server.listen(PORT, HOST, () =>
    console.log(`Server listen on ${HOST}:${PORT}.`)
);
