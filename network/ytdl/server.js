import express from 'express';
import path from 'path';
import fs from 'fs';

/* Configuration */
const config = {
    publish: {
        host: '0.0.0.0',
        port: 3120
    }
};

/* Initialize the server */
const app = express();

app.get(/^\/?ytdl$/, (req, res) => {
    res.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
    res.write(
        JSON.stringify({
            success: true,
            error: false,
            data: 'download'
        })
    );
    res.end();
});

app.get('*', (req, res) => {
    res.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
    res.write(
        JSON.stringify({
            success: true,
            error: false
        })
    );
    res.end();
});

app.listen(config.publish.port, () => {
    console.log(
        `Server listening on http://${config.publish.host}:${config.publish.port}.`
    );
});
