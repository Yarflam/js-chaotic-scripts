class PerfCode {
    constructor() {
        this._sources = [];
        this._pass = 5;
        this._msts = 2;
    }

    source(code) {
        if (typeof code !== 'function') return false;
        this._sources.push(this._env(code));
        return this;
    }

    run({ begin, running, finish }) {
        let stats = { eval: [] };
        /* Evaluate */
        for (
            let ts, script, pass, ope, imax = this._sources.length, i = 0;
            i < imax;
            i++
        ) {
            if (typeof begin === 'function') begin(i + 1);
            script = this._sources[i];
            stats.eval.push([]);
            pass = 0;
            while (pass < this._pass) {
                if (typeof running === 'function')
                    running(pass + 1, this._pass);
                ts = this.ts();
                ope = 0;
                while (this.ts() - ts < this._msts * 1000) {
                    script();
                    ope++;
                }
                stats.eval[i].push(ope / this._msts);
                pass++;
            }
        }
        /* Stats */
        stats.ope = stats.eval.map(values =>
            Math.floor(
                values.reduce((accum, x) => accum + x, 0) / values.length
            )
        );
        // stats.maxOpe =
        /* Output methods */
        if (typeof finish === 'function') {
            stats.ope.forEach((ope, index) =>
                finish({ index: index + 1, ope })
            );
        }
        return stats;
    }

    ts() {
        return new Date().getTime();
    }

    _env(fct) {
        let obj = {};
        obj.fct = new Function('return (' + fct.toString() + ').bind(this)();');
        return () => obj.fct();
    }
}

module.exports = { default: PerfCode };
