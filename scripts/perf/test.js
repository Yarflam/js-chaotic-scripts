const PerfCode = require('./PerfCode').default;

(() => {
    /* Initialize */
    const app = new PerfCode()
        .source(function() {
            /* Loop */
            let data = new Array(2000).fill(0).map(() => Math.random());
            let sum = 0;
            for (let imax = data.length, i = 0; i < imax; i++) {
                if (!(data[i] > 0.5)) continue;
                sum += data[i] + 1;
            }
            // console.log(sum);
        })
        .source(function() {
            /* VS Reduce */
            let data = new Array(2000).fill(0).map(() => Math.random());
            const sum = data.reduce((accum, x) => {
                if (!(x > 0.5)) return accum;
                return accum + (x + 1);
            }, 0);
            // console.log(sum);
        });
    /* Test */
    let output = app.run({
        begin: index => console.log(`[${index}] Start`),
        running: (pass, max) => console.log(`Pass ${pass}/${max}`),
        finish: ({ index, ope }) => console.log(`[${index}] ${ope} ope/s`)
    });
    // console.log(output);
})();
