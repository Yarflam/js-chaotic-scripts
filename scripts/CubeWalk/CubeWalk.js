/* Cube Walk - What is the best path? */

/* Distance 3D vector */
function dist(a, b) {
    return Math.sqrt(
        (b[0] - a[0]) * (b[0] - a[0]) +
            (b[1] - a[1]) * (b[1] - a[1]) +
            (b[2] - a[2]) * (b[2] - a[2])
    );
}

/* My points 3x3x3 = 27 */
let points = [
    { p: [0, 0, 0] }, // center
    // { p: [1, 0, 0] }, // right
    { p: [1, 1, 0] }, // behind-right
    // { p: [0, 1, 0] }, // behind
    { p: [-1, 1, 0] }, // behind-left
    { p: [-1, 0, 0] }, // left
    { p: [-1, -1, 0] }, // front-left
    { p: [0, -1, 0] }, // front
    { p: [1, -1, 0] }, // front-right
    { p: [0, 0, 1] }, // top-center
    { p: [1, 0, 1] }, // top-right
    { p: [1, 1, 1] }, // top-behind-right
    { p: [0, 1, 1] }, // top-behind
    { p: [-1, 1, 1] }, // top-behind-left
    { p: [-1, 0, 1] }, // top-left
    { p: [-1, -1, 1] }, // top-front-left
    { p: [0, -1, 1] }, // top-front
    { p: [1, -1, 1] }, // top-front-right
    { p: [0, 0, -1] }, // bottom-center
    { p: [1, 0, -1] }, // bottom-right
    { p: [1, 1, -1] }, // bottom-behind-right
    { p: [0, 1, -1] }, // bottom-behind
    { p: [-1, 1, -1] }, // bottom-behind-left
    { p: [-1, 0, -1] }, // bottom-left
    { p: [-1, -1, -1] }, // bottom-front-left
    { p: [0, -1, -1] }, // bottom-front
    { p: [1, -1, -1] } // bottom-front-right
];
points.sort((a, b) => (Math.random() >= 0.5 ? -1 : 1));

// start
points.unshift({ p: [1, 0, 0] }); // right
// end
points.push({ p: [0, 1, 0] }); // behind

/* Generate the links */
for (let i = 0; i < points.length; i++) {
    for (let j = i + 1; j < points.length; j++) {
        if (dist(points[i].p, points[j].p) <= 1) {
            if (!points[i].lk) points[i].lk = [];
            if (!points[j].lk) points[j].lk = [];
            points[i].lk.push(j);
            points[j].lk.push(i);
        }
    }
}

/* Simulate - Backtracking algorithm */
let chain = [0];
while (chain.length && chain.length < points.length) {
    let slct = points[chain[chain.length - 1]];
    slct.wk = slct.wk || 0;
    /* Next/Previous point */
    let nextPt = null;
    for (; slct.wk < slct.lk.length; slct.wk++) {
        if (chain.indexOf(slct.lk[slct.wk]) < 0) {
            nextPt = slct.lk[slct.wk];
            slct.wk++;
            break;
        }
    }
    if (nextPt) {
        chain.push(nextPt); // next
    } else {
        slct.wk = 0; // reset
        chain.pop(); // previous
    }
    console.log(Buffer.from(chain).toString('hex'));
}

/* Soluce */
if (chain.length) {
    console.log(`\nSolution (${chain.length}/${points.length}): `);
    for (let idx of chain) {
        console.log(`(${points[idx].p.join(', ')})`);
    }
}
