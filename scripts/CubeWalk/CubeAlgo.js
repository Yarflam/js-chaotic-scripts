/* Straight Line */
const SCHEM_SL = [
    [1, 0, 0],
    [0, 0, 0],
    [0, 0, -1],
    [-1, 0, -1],
    [-1, 1, -1],
    [0, 1, -1],
    [1, 1, -1],
    [1, 0, -1],
    [1, -1, -1],
    [0, -1, -1],
    [-1, -1, -1],
    [-1, -1, 0],
    [-1, -1, 1],
    [-1, 0, 1],
    [0, 0, 1],
    [0, -1, 1],
    [0, -1, 0],
    [1, -1, 0],
    [1, -1, 1],
    [1, 0, 1],
    [1, 1, 1],
    [1, 1, 0],
    [0, 1, 0],
    [0, 1, 1],
    [-1, 1, 1],
    [-1, 1, 0],
    [-1, 0, 0]
];

/* Corner */
const SCHEM_CR = [
    [1, 0, 0],
    [1, 0, 1],
    [1, -1, 1],
    [1, -1, 0],
    [1, -1, -1],
    [0, -1, -1],
    [0, 0, -1],
    [1, 0, -1],
    [1, 1, -1],
    [0, 1, -1],
    [-1, 1, -1],
    [-1, 0, -1],
    [-1, -1, -1],
    [-1, -1, 0],
    [0, -1, 0],
    [0, 0, 0],
    [0, 0, 1],
    [0, -1, 1],
    [-1, -1, 1],
    [-1, 0, 1],
    [-1, 0, 0],
    [-1, 1, 0],
    [-1, 1, 1],
    [0, 1, 1],
    [1, 1, 1],
    [1, 1, 0],
    [0, 1, 0]
];

/* Move */
const RULE_MOVES = {
    A: [-1, 0, 0],
    B: [0, -1, 0],
    C: [0, 0, -1],
    D: [1, 0, 0],
    E: [0, 1, 0],
    F: [0, 0, 1]
};

function vectDiff(a, b) {
    return [b[0] - a[0], b[1] - a[1], b[2] - a[2]];
}

function getAlgo(schem) {
    let out = [];
    const codeMove = Object.keys(RULE_MOVES);
    const diffMove = Object.values(RULE_MOVES).map(x => x.join(','));
    for (let i = 0; i < schem.length - 1; i++) {
        let [a, b] = schem.slice(i, i + 2);
        let code = codeMove[diffMove.indexOf(vectDiff(a, b).join(','))];
        out.push(code);
    }
    return out;
}

console.log(getAlgo(SCHEM_SL).join(''));
console.log(getAlgo(SCHEM_CR).join(''));
